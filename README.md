# Adding HTML list support for Android TextView

This is a supporting sample project for my answer <http://stackoverflow.com/a/17365740/262462>.
Initial TagHandler code is taken from my commits at <https://bitbucket.org/saibotd/bitbeaker> after tag `release-v2.4.1`.

If you find the codes in this repo useful, please vote my SO answer up. Improvements via pull requests are greatly appreciated.
